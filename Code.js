// Config vars
TEMPLATE_DOC_ID = "1syTKKGw9IQc-7uSxCjHfLz0xFTXkt0v8zDub_zd7y84"
FOLDER_ID = "1_ySK_tp-ogWuMCOwWI9t_tOQcKlgL4x2"
EMAIL_RECIPENT = "niehaus.1301@gmail.com"
CLIENT_ID = '372488105238-v3il767jkqppgk61948mdtuffidl4oi7.apps.googleusercontent.com'
CLIENT_SECRET = 'W1q4_2Ui6TORy_s9s8yPOqHv'



// Main function which triggers on form submit
function handler(data) {

  Logger.log(JSON.stringify(data.values))
  values = data.values
  
  

  // Get todays date
  var today = getDate()

  // Evaluate form results and generate text
  if (values[1] === "Yes") {
    var title = today
    var text = "am " + today
    var plain = today 
  } else {
    if (values[3] === "") {
      var title = values[2]
      var text = "am " + values[2]
      var plain = values[2]
    } else {
      var title = values[2] + " - " + values[3]
      var text = "vom " + values[2] + " bis zum " + values[3]
      var plain = values[2] + " bis zum " + values[3]
    }
  }
  
  if (!(values[5] === "")) {
    var text = text + ", " + values[5] + " Stunde"
  }

  if (!(values[4] === "")) {
    var text = text + " aufgrund von " + values[4]
  }

  // Copy template and open copy
  var template = DriveApp.getFileById(TEMPLATE_DOC_ID);
  var destFolder = DriveApp.getFolderById(FOLDER_ID);
  var copy = template.makeCopy(title, destFolder).getId();
  var doc = DocumentApp.openById(copy)
  var body = doc.getBody()

  // Replace placeholders with data
  body.replaceText('{{date}}', today);
  body.replaceText('{{timereason}}', text);

  doc.saveAndClose()
  

  // Send confirmation email
  // sendMail(doc.getUrl(), plain)
  
  // Print doc
  printGoogleDocument(copy, '1581be5e-30d3-65a8-fb9b-fd9d675e3097', title)
}





